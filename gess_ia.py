#!/usr/local/bin/python

# coding: UTF-8

'''
# Description:
    IA qui joue au gess.
    Avec un peu de modularité, de l'adaptation de notion de niveau et surtout une nouvelle fonction de score, on devrait lui faire jouer d'autres jeux.
    Les deux fonctions principal de calcul sont
        deriver une sitation: regarder tous les coups possibles a partir de celle la et evaluer le score de chacune de ces situations.
        dealResults qui integre les resultats d'une derivation dans l'arbre de decision.
    Les derivations sont executees par des process qui tournent en parallele. Les inetegrations de resultats sont centralisees pour ne pas avoir de bugs d'acces concommitents.
    Les situations filles d'une mere sont toujours triees par score croissant (un score de situation positif est a l'avantage de blanc; negatif a l'avantage de noir)
# Licence:
   GPL V3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

 Auteur:
   perdrive@gmx.com

# Version: 1.2.0
    Release: 31/01/2023

# Nouveautés:
    integration IA en mesure de jouer via gess.py
        constructeur IA prend en parametre un fct par laquelle elle envoie ses coups.
    arreter de garder les grilles des situations. la calculer au besoin. (enorme gain ram)
    decision critere fin reflexion: nb derivations à son tour, calculé depuis lvl IA
        puis updaté au long de la partie (derivs plus simples -> en faire plus)
    pruner selon coups joués (grands parents <a cause undo> qui ne sont pas descendants dignes d'interet.)
    Refonte des process de derivation pour soigner le "recursion error... picklings"
       -> Process long terme. Qin+qOut (cf ~/dev/cours/multiprocessing/testMultiProcessing2.py)
    Fonction score:
        ne plus compter les pieces
        compter la distance au(x) roi(s) adverse(s)
    Correction bugs variés en fin de partie

# TODO pas optionnel:
    reste du bug en fin de partie. (detection arbre decidé)?
    fix bug removeFille
    fix bug load game & IA
    fix bug derivsAttendues & Undo
    fix bug derivsAttendues & racine non derivee
    quand sit passe en derivation, updater le score de son parent  pour negliger filles en cours de derivationet parcours arbre remontant
    Pitin de cpu_count()
        Il ne faut pas compter dessus. Le determiner avec un benchmark puis coller la bonne valeur a la variable nbProcToUse
# TODO (optionnel):
   Ameliorer fct score: Fin de parties pas top. Pistes:
       -ajouter criter "trait"
       -ne prendre en compte que 2/3 ou 1/2 des degats effectués pour reduire les allers et retours entre branches (test)
       -ajouter critere "controle du centre" en debut/milieu de partie
   pruner selon ram (pret? a brancher si necessaire. Est ce?)
       casser liens entre old rac et ses parents si parent a plus de pierres d'au moins 1 couleur.
       ya t il un lien de nouv rac vers parents old rac?
           parcourir tout l'arbre (nouds dérivés seulements)
           les noeuds dérivés non marqués sont prunés ainsi que leurs filles non dérivées qui n'ont pas de parents conservés.
'''


from gess_rules import *
from gess_basics import *

from multiprocessing import Queue,Process     #calculs de dernivations sur procos dédiés
from threading import Thread                            #IA dans un thread pour tourner avec une interface
from os import getpid                                   #recuperation des pid pour associer les params d'un thread a ses resultats
#from psutil import virtual_memory as mem                #detection memoire vive->dureté pruning arbre decision
from random import choice as rnd_choice                 #choix prochain coup dans une liste (cas egalité)
from time import time,sleep



nbProcToUse = 4   #pitin de cpu_count()

#statuts de derivation possible d'une Situation:
DRV_NON     = 0
DRV_ENCOURS = 1
DRV_OUI     = 2
DRV_DECIDE  = 3

class Situation:
    def getBestMouvNoir(_,i=0):
        return(_.mouvTries[i])
    def getBestMouvBlanc(_,i=0):
        return(_.mouvTries[-i-1])
    def __init__(_,grille,calcScore=True):#,nodeDepth):
        _.id = grille2id(grille)
        _.parents = []  #liste de noeuds
        _.filles =  {} #les clés sont des deplacements, les valeurs sont des noeuds ou des id
        _.mouvTries=[]#None #sont des moves, tries par scores croissants
        #_.best=max if grille.joueur==BLANC else min
        _.getBestMouv= _.getBestMouvBlanc if grille.joueur==BLANC else _.getBestMouvNoir
        _.victoire=INDE6
        _.status=DRV_NON
        _.score=None
        if calcScore:
            _.calcScore(grille)
    def __str__(_):
        return("[Sit. Id: %s ]"%hex(_.id))
    def getJoueur(_):
        return(sgn(_.id))
    def getBestFille(_,i=0):
        try:
            return(_.filles[_.getBestMouv(i)])
        except:
            print("cant getBestFille pos",i,"among",len(_.mouvTries))
    def getBestDeriv(_):
        if _.status==DRV_ENCOURS:
            return(False)   #on ne connait pas encore ses descendants. Attendre.
        if _.status==DRV_DECIDE:
            return(None)    #cette fois ci ya plus rien a faire
        if _.status==DRV_NON:
            return(_)   #bon candidat
        refuse={DRV_DECIDE,DRV_ENCOURS} #on ne cherche pas de descendants de telles sit
        accepte={DRV_NON}   #on ne cherche que ces sit la
        sit=_
        i=0
        sitSansDEC=True
        pile=[] # contient: (sit,i en cours,sitSansDEC)
        excludes=set()  #les sits par lesquelles on va tenter de passer (ne pas y repasser hors depilement)
        while True:
            fille=sit.getBestFille(i)
            #cas passer a la soeur ou au parent si derniere fille
            if fille in excludes or fille.status in refuse:    #ne pas passer par des situations en examen; ne pas boucler.
                if fille.status==DRV_ENCOURS:
                #    aucunDEC=False
                    sitSansDEC=False
                i+=1
                #depiler au besoin si on en a fini avec sit cour
                while i>=len(sit.mouvTries):
                    #rien ne va.Peut on depiler? (while car potentiellement plusieurs depilage d'affilee)
                    if len(pile)==0:
                        #return(aucunDEC)
                        return(False)
                    sit,i,sitSansDEC=pile.pop()
                    #excludes.remove(sit)
                    i+=1
            #cas renvoyer fille
            elif fille.status in accepte:
                return(fille)
            #cas tester les filles de la fille.
            else:
                #print("\ton empile la mere, voyons la fille",fille.id)
                pile.append((sit,i,sitSansDEC))
                sit=fille
                i=0
                sitSansDEC=True
    #cette fct ne doit pas etre appelee apres derivation
    def calcScore(_,grille):
        if _.score==None: #on fait les calculs
            _.score=calcScore(grille)
        if abs(_.score)>=VICTOIRE:
            _.status=DRV_DECIDE
            _.victoire=sgn(_.score)
        return(_.score)
    #met a jour les scores du noeux et de ses parents
    def majScores(_):
        if _.status==DRV_NON:
            debugDisplayGrille(id2grille(_.id),["pourquoi est il non derivé?","score: "+str(_.score),"status: "+str(_.status)])
            assert(False)
            return
        elif _.status==DRV_ENCOURS:
            todoes=_.parents.copy()
        else:
            todoes=[_]
        while todoes:
            sit=todoes.pop()
            fille=sit.getBestFille()
            #TODO: negliger filles DRV_ENCOURS. Si toutes filles en cours?
            score=fille.score       #recup score meilleur fille
            if score!=sit.score:    #il faut mettre score de sit a jour 
                sit.score=score
                todoes+=sit.parents #donc potentiellement ses parents
                #pour chaque parents, retrier ses filles
                for parent in sit.parents:
                    mv=parent.getMouv(sit)
                    oldPos=parent.mouvTries.index(mv)
                    triRequis=False
                    if oldPos>0:
                        oldScorem=parent.filles[parent.mouvTries[oldPos-1]].score
                        if oldScorem > score:
                            triRequis=True
                    if not triRequis and oldPos+1!= len(parent.mouvTries):
                        oldScoreM=parent.filles[parent.mouvTries[oldPos+1]].score
                        if oldScoreM < score:
                            triRequis=True
                    if triRequis:
                        parent.mouvTries.pop(oldPos)
                        newPos=parent.getScorePos(score)
                        parent.mouvTries.insert(newPos,mv)
    #renvoie l'indice ou inserer un score dans les filles triees croissants
    def getScorePos(_,score):
        if len(_.mouvTries)==0:
            return(0)
        def getScore(i):
            return(_.filles[_.mouvTries[i]].score)
        m=0
        if score<=getScore(m):
            return(m)
        M=len(_.mouvTries)-1
        if score>=getScore(M):
            return(M+1)
        while M-m>1:
            mid=(m+M)//2
            scoreMid=getScore(mid)
            if scoreMid<score:
                m=mid
            elif scoreMid>score:
                M=mid
            else:
                return(mid)
        return(M)
    def addFille(_,mv,fille):
        parentConnu=_ in fille.parents
        if parentConnu:
            print("ajout parent inutile",_.id,fille.id,mv)
        else:
            fille.parents.append(_)
        assert( not mv in _.filles) #Etait un if. Me paris pas utile de le verifier. Insere assert pour verif.
#            return(None)    #la fille n'est pas ajoutée
        _.filles[mv]=fille
        pos=_.getScorePos(fille.score)
        _.mouvTries.insert(pos,mv)
    def removeFille(_,mvk):
        fille=_.filles[mvk]
        assert(_ in fille.parents)
        del _.filles[mvk]
        fille.parents.remove(_)
        pos=_.getScorePos(fille.score) #TODO: BUG egalité de score n'est pas egalité de fille.'
        _.mouvTries.pop(pos)
        return(pos)
        #chkReduceScore inutile: on ne remove jamais les bestFilles
    def getMouv(_,fille):
        for mvk in _.filles:
            if _.filles[mvk]==fille:
                return(mvk)
        assert(False)

VICTOIRE=1000


def calcScore(grille):
#        print("new")
    plr=grille.joueur
    enmi=-1*plr
    ringsEnmi=grille.getRings(enmi)
    ringsPlr=grille.getRings(plr)
    #on teste d'abord si l'ennemi a toujours un roi, ie s'est il suicidé au tour precedent?
    if len(ringsEnmi)==0:
        return(VICTOIRE*plr)
    elif len(ringsPlr)==0:
        return(VICTOIRE*enmi)
    score=0
    #nombre de pierres
    #fct doit montrer pas qu'echange est defarorable pour perdant
    prb=grille.getPierres(BLANC)
    prn=grille.getPierres(NOIR)
    score+=(prb-prn)*200/max(prb,prn)#entre + et -700. Une pierre represente entre 4 et 25 points a peu pres
    #distances au(x) roi(s).  On va ramener la valeur max a +/-8 (comme 2 pierres a leur minimum)
    #initialiser des sommes de dist par roi:
    dists=[None,[0]*len(grille.getRings(BLANC)),[0]*len(grille.getRings(NOIR))]
    for i in range(18):
        for j in range(18):
            clr=-grille.getVal(i,j) #couleur (opposee a la pierre trouvee)
            if clr:
                lr=grille.getRings(clr)
                ld=dists[clr]
                for r in range(len(lr)):
                    k=lr[r]
                    ld[r]+=max(abs(i-k[0]),abs(j-k[1]))
    #une fois les sommes en moyennes, on obtiendra des dists moyennes pierre au roi ennemi entre 2 et 17
    #ca fait un delta dist entre -15 et 15. On divise par 2 pour ramener a +/- 8.
    # test On passe a sum au lieu de max et div 4 au lieu 2 ci dessous
    score-=sum(dists[NOIR])/4/prb
    score+=sum(dists[BLANC])/4/prn
    return(score)


def serialDeriv(procid,qIn:Queue,qOut:Queue):
    while(True):
        #lire param d'appel dans qIn
        while qIn.empty():
            sleep(0.001)
        try:
            question=qIn.get(0)
        except:
            continue
        #si c'est un ordre de suicide(None), mtrre None dans qOut et suicide
        if question==None:
            qOut.put(procid)    #donne son id
            break               #avant de se suicider
        #mettre le res derivation (grid , (depart,arrivee,sitFille)* ) dans qOut
        deriv(question,qOut)    #on cherche la reponse a la question et on pose la reponse dans qOut


def deriv(grid,queue):
#    t=time()

    idNew=set()    #ensemble des id des nouvelles grilles generees
    retval=[]
    grille=id2grille(grid)
    plr=grille.joueur
    lfilles=[]
    yavictoire=False #arreter la recherche si on trouve une victoire
    #construire la liste des pieces (au besoin) pour pouvoir deriver
    lp=grille.getPieces(plr)
    grille.getPierres(0)
    pierres=grille.pierres
    #chercher tous les coups possibles
    for depart in lp:    #pour chaque piece
        grille.selectPiece(depart)
        lm=grille.mouvementsPSel()
        for arrivee in lm:
            if arrivee in grille.collisPSel.keys():
                collisions=grille.collisPSel[arrivee]
            else:
                collisions=[]
            g=Grille(grille)
            g.move(arrivee)#grille de la situation fille
            #Si on tombe sur un noeud deja connu dans le graphe: On conserve l'ancien
            #ne conserver qu'un chemin d'un parent vers une fille
            fid=grille2id(g)
            if  fid in idNew:
                continue
            idNew.add(fid)
            g.getPierres(0)    #calcule ses rings et ses nombres de pierres
            nouvPierres=g.pierres
            if pierres[plr]-nouvPierres[plr]>2 and pierres[-plr]==nouvPierres[-plr]:
                continue    #on neglige les filles qui perdent au moins 3 pierres sans en tuer
            if g.getRings(plr)==[]:
                continue    #on neglige les suicides
            fille=Situation(g)
            if fille.score*plr>=VICTOIRE:
                fille.victoire=plr  #situation.victoire=
                yavictoire=True
                retval=[(depart,arrivee,fille)]
                break
            else:
                retval.append( (depart,arrivee,fille) )
        if yavictoire:
            break
#    print("tderivNew",time()-t)
    if not queue:
        return(retval)
    retval.insert(0,grid)
    queue.put(retval)



class IA(Thread):
    def __init__(_,grille,envoiCoup,levels):
        Thread.__init__(_)
        #mem check
        #_.memRef=mem().free
        assert(_.memRef>1<<29) #Requiert 512Mo RAM libre au lancement. SINON, en tant que root: echo 3 > /proc/sys/vm/drop_caches
        #champs liés aux params
        _.racine=_.futureRacine=Situation(grille)
        _.envoiCoup=envoiCoup   #fonction a laquelle envoyer les coups a jouer.
        _.levels=[None]         #Va contenir le nombre de derivations sur son temps de reflexion sous la forme [None,50,None] si seule IA blanche
        for i in (1,0):
            if levels[i]==None:
                _.levels.append(None)
            else:
                _.levels.append(int(25*1.2**levels[i])) #nbDeriv=30 au niv 1; 62 au niv 5; 154 au niv 10; 385 au niv 15; 954 au niv 20
        #champs liés a la force de l'IA
        _.durInit=[None]*3      #contiendra la duree du premier tour des ia
        _.durPrev=[None]*3      #contiendra la duree du precedent tour de l'ia'
        #champs liés a l'arbre de decision
        _.hNodes={_.racine.id:_.racine}
        #champs liés aux process
        _.qOut=Queue()
        _.qIn=Queue()
        _.lProcs=[] #liste des processus
        for i in range(nbProcToUse):
            _.lProcs.append(Process(target=serialDeriv,args=(i,_.qIn,_.qOut)))
        tderiv=time()  #utilisé
        _.racine.status=DRV_ENCOURS
        retval=deriv(_.racine.id,None)
        _.tderiv=time()-tderiv  #le temps d'une deriv unitaire determine les durees de sleep.
        _.dealResults(_.racine,retval)  #associe ses filles a la racine
        _.computing=False       #sera passé a True dand run()
    def slowKill(_):
        if _.computing:
            _.computing=False
            #pousser les processus au suicide :
            for i in range(nbProcToUse):   #pour chaque processus:
                _.qIn.put(None)             #mettre un none=suicide dans qIn
        if _.lProcs:
            print("attente derniers resultats et tuage des prcocessus")
            lProcs=_.lProcs
            _.lProcs=None
            i=nbProcToUse
            while(i):   #tant qu'il y a des processus a tuer:
                if _.qOut.qsize():
                    try:
                        res=_.qOut.get(0)    # au format:[pid,(mv,fille)*]
                    except: #la queue est illisible
                        continue    #on va retenter de lire la queue (mm elt)
                    if type(res) is int:
                        while True:             #boucle pr tuer le proc (do it dammit)
                            try:                    #ca marche pas a tous les coups
                                lProcs[res].close()   #on tente de tuer le proc sans erreur
                                i-=1                    #on note "un de fait"
                                break                   #on sort de la boucle "do it dammit"
                            except:                 #ca a pas marché
                                sleep(0.001)            #on re essaie (do it dammit)
                else:   #la queue est vide, on attend.
                    sleep(_.tderiv/10) #si qOut est vide
            print("derniers resultats obtenus. IA morte.")
    def pruneExceptSubTree(_,situations:set):
        toDel=set(_.hNodes.keys())
        while len(situations):
            sit=situations.pop()
            toDel.remove(sit.id)
            for m in sit.filles:
                f=sit.filles[m]
                if f.id in toDel:
                    situations.add(f)
        for id in toDel:
            del(_.hNodes[id])
        #virer liens de parenté vers noeuds supprimés
        for id in _.hNodes:
            sit=_.hNodes[id]
            i=0
            while(i<len(sit.parents)):
                p=sit.parents[i]
                if p.id in toDel:
                    sit.parents.pop(i)
                else:
                    i+=1
    #incorporer les resultats d'une derivation dans l"IA
    def dealResults(_,mere,results):
        #drop les resultats si mere prunee
        if not mere.id in _.hNodes:
            return
        if not mere.status==DRV_ENCOURS:
            debugDisplayGrille(id2grille(mere.id),["Bizarre, le status n'est pas ENCOURS","status: "+str(mere.status)])
        mere.status=DRV_OUI
        for depart,arrivee,fille in results:
            mv=da2mv(depart,arrivee)
            if fille.id in _.hNodes:
                fille=_.hNodes[fille.id]
            else:
                _.hNodes[fille.id]=fille
            mere.addFille(mv,fille) #deriv n'a pas ramené 2 mvts amenant a 1 situation -> pas de pb de doublons
    #setRacine gere les questions prep nb coups avant de jouer
    def changeRacine(_):
        #si pas derivé:
        if _.futureRacine.status==DRV_NON:
            #faire une premiere derivation. Ca va prendre du temps sur le processus du moteur!!! TODO: le cas ne doit pas etre vu ici mais dans run()
            _.futureRacine.status=DRV_ENCOURS
            retval=deriv(_.futureRacine.id,None)
            _.dealResults(_.futureRacine,retval)  #associe ses filles a la racine
            _.futureRacine.status=DRV_OUI
            #TODO:      je prends double temps! (facile si mon tour... sinon?)
        (vieilleRacine , _.racine) = (_.racine , _.futureRacine)
        #print("avant prune")
        _.pruneExceptSubTree(set( (vieilleRacine,_.racine) ))
        #print("apres prune")
    #appelé quand nouvelle situation envoyee par le jeu.
    def recoitSGI(_,x):
        #le param peut etre grille, sit, id. Le mieux est une situation. Doit etre rapide car appelee externalement (par UI)
        if type(x) is Grille:
            id=grille2id(x)
        elif type(x) is Situation:
            id=x.id
        else:
            id=x
        if id in _.hNodes:
            rac=_.hNodes[id]
        else:
            #calc situation (sans score)
            if type(x) is Situation:
                rac=x
            else:
                if type(x) is Grille:
                    g=x
                else:
                    g=id2grille(x)
                rac=Situation(g,False)
            #sauver sit dans hNodes
            _.hNodes[id]=rac
        _.futureRacine=rac
    def getBestMouv(_,grid=None):
        """Renvoie le meilleur coup selon une grille. Celle ci doit etre connue et avoir au moins une fille derivée.
        En cas d'egalité entre filles derivees, on choisit en random
        Renvoie False en cas de probleme.
        Param: id de la grille ou rien pour la racine de l'ia"""
        def convient(sit):
            """fct local prenant une sit mere en param et renvoyant si acceptable (soit derivée soit victoire)"""
            return (sit.status not in {DRV_NON,DRV_ENCOURS} or sit.score*sit.getJoueur()>=VICTOIRE)
        if grid==None:
            sit=_.racine
        elif grid in _.hNodes.keys():
            sit=_.hNodes[grid]
        else:
            return(False)
        #trouver meilleure situation fille ayant ete derivée
        cond=False
        for i in range(len(sit.mouvTries)):
            bestie=sit.getBestFille(i)
            cond=convient(bestie)
            if cond:
                break
        if not cond:
            return(False)
        #trouver les situations derivees avec meme score
        scref=bestie.score
        possibles=[i]
        for i in range(i+1,len(sit.mouvTries)):
            nextBestie=sit.getBestFille(i)
            if scref!=nextBestie.score:
                break
            if convient(nextBestie):
                possibles.append(i)
        #en choisir une en random
        return(sit.getBestMouv(rnd_choice(possibles)))
    def playBestMouv(_):
        """Envoie le meilleur coup trouvé au moteur.
        renvoie False si pas de bon coup a jouer. Faudrait savoir!. Est ce seulement recupéré?"""   #TODO
        assert(_.levels[_.racine.getJoueur()])
        mv=_.getBestMouv()
        if mv==False:
            print("IA ne veut pas jouer. pas trouvé de filles derivees")
            return(False)   #devrait renvoyer un tempsRestantEstimé
        dep,arr=mv2da(mv)
        if _.envoiCoup(dep,arr): #TODO: chk gess UI renvoie un booleen
            _.futureRacine=_.racine.filles[mv]  #on se prepare au changement de racine
            return(True)
        print("IA  a vu son coup refusé par le jeu")
        return(False)
    def run(_):
        """Cette methode est heritée de Thread. Si elle est appelée directement, elle ne sera pas threadee.
    A la place, il faut appeler la methode start() qui lance run dans un thread séparé"""
        _.computing=True
        for proc in _.lProcs:
            proc.start()
        derivsAttendues=_.levels[_.racine.getJoueur()]
        print("derivsAttendues",derivsAttendues)
        derCeTour=0
        derParSec=None
        coeffNbDer=1        #coefficient multiplicateur lié a la simplification des situations au fur et a mesure de la partie.
        debutTour=time()
        while _.computing:
            slp=True
            #faut-il lancer un calcul?
            if _.qIn.qsize()<nbProcToUse:
                #TODO: verif racine resolue (score victoire). A t elle des filles en cours der?
                if abs(_.racine.score)>=VICTOIRE:
                    if derivsAttendues:
                        derivsAttendues-=1
                else:
                    sit=_.racine.getBestDeriv()
                    if sit:
                        slp=False
                        _.qIn.put(sit.id)
                        sit.status=DRV_ENCOURS
                        #TODO faire une mise a jour du score des parents car son score est invalide.
            #faut il gerer des resultats?
            if _.qOut.qsize():
                slp=False
                try:
                    res=_.qOut.get(0)    # le res est au format:[pid,(mv,fille)*]
                except: #la queue est vide
                    continue
                grid=res.pop(0)
                derCeTour+=1
                #sit=path[-1]
                try:
                    sit=_.hNodes[grid]
                except:
                    print("on avait dit qu'on purgeait aps les sits en derivation, merde!")
                    continue
                _.dealResults(sit,res)
                sit.majScores()
                if derivsAttendues: #ni None ni 0
                    derivsAttendues-=1
            #jouer un coup si attendu:
            if derivsAttendues==0:
                _.playBestMouv()        #choisit un coup, l'envoie au jeu et fixe futureRacine. Implique ci dessous.
            #Acter un coup joué (par soi(IA) ou par le jeu(humain))
            if _.racine!=_.futureRacine:
                slp=False
                joueur=_.futureRacine.getJoueur()
                derivsAttendues=_.levels[joueur]
                _.changeRacine()
                t=time()
                durTour=t-debutTour
                if derCeTour>=10:   #si nombre de derivees significatif:
                    if derParSec==None:
                        #initialiser le compte de derivees par sec. Ne doit plus bouger.
                        derParSec=derCeTour/durTour
                    else:
                        coeffNbDer=derCeTour/durTour/derParSec
                if derivsAttendues:
                    derivsAttendues=int(derivsAttendues*coeffNbDer)
                print("un coup a ete joué en ",durTour,". l'IA attend des derivations?",derivsAttendues)
                debutTour=t
                derCeTour=0
            #vider le la ram au besoin
            # TODO
            #attendre si pas de nouveaux resultats a traiter
            if slp:
                sleep(_.tderiv/10)
        #sorti du while. tuer les processus
        _.slowKill()
        #attente des derniers resultats



def printCoup(depart,arrivee):
    print("IA joue "+mv2str(depart,arrivee))

