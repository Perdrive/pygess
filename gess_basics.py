# -*- coding: utf-8 -*-

'''
# Description:
   Quelques constantes de jeu et fonctions tres generiques.

# Licence:
   GPL V3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

# Auteur:
   perdrive@gmx.com

# Version: 1.1
    Release: 31/01/2023

# Nouveautés:
    amelioration vraieCopie
'''

## Quelques constantes:
BLANC=1
NOIR=-1
PAT = None
INDE6=0 #"indecis" lol


##fonction manquante de python
def sgn(a):
    """renvoie 0,1,-1"""
    try:
        return(round(a/abs(a)))
    except:
        return(0)


## calculs basiques sur tuples consideres comme vecteurs
def vectDiff(orig,dest):
    """renvoie le tuple (dest-orig)"""
    return(tuple(dest[i]-orig[i]for i in range(len(dest))))

def vectAdd(u,v):
    """renvoie le tuple (dest-orig)"""
    return(tuple(u[i]+v[i]for i in range(len(u))))

def vectMult(v,k):
    return(tuple(elt*k for elt in v))

#fonction recursive (attention profondeur) de copie d'objets avec copie des sous objets.
def vraieCopie(x):
    """renvoie une vraie copie d'une [liste|tuple|dict] contenant des [idem|atom]
    Ainsi, modifier l'un ne modifie pas l'autre.
    Attention: python n'aime pas les longues recursions."""
    if type(x) is list:
        return([vraieCopie(y) for y in x])
    if type(x) is set:
        return(set(vraieCopie(y) for y in x))
    if type(x) is tuple:
        return(tuple(vraieCopie(y) for y in x))
    if type(x) is dict:
        return({vraieCopie(y):vraieCopie(x[y])  for y in x.keys()})
#    if "copy" in dir(type(x)):  #si l'objet a une methode "copy", l'utiliser
#        finalement non. C'est tres lent!'
#        return(x.copy())    #copie des sous objets potentiels non faits.
    return(x)

