#!/home/pyenvs/pygame/bin/python
# coding: UTF-8


'''
# Description:
   GUI pour gess.py
   Soit en PVP, soit en PVIA, ou meme IAVIA. En l'absence de menu, aller a la fin du code et choisir
        "IA_<nombre>" comme nom d'un joueur pour un joueur géré par la machine.
            Le nombre > 0 designe le niveau de l'IA. Le niveau et la puissance du CPU impliquent la duree du tour de l'IA
        autre chose pour un joueur humain


# Licence:
   GPL V3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

 Auteur:
   perdrive@gmx.com

# Version: 1.2.0
    Release: 31/01/2023

# Nouveautés:
    integration IA en mesure de jouer via gess.py
    fin de partie un peu moins moche

# TODO (par urgence decroissante):
    fix bug load game et ia!=None
    fix timer IA (truc chelou si on joue vite.)
        min nb init
#   tracer coords en bas et a droite du goban
#   se debarasser des interactions via console
#   Gerer le pat (ne restent que 2 rois pendant 2 tours)
#   afficher des croix pour les pierres qui vont mourir (ou les virer de l'affichage?)
#       picture = pygame.transform.scale(picture, (1280, 720)) pour scaler une image de croix
#   tourner le goban selon player
#       anim retourn
#   welcomescreen/victorySCreen/menu
#   demander confirm pour coups autodestructeurs
#   horloge
#   menu game
#       new
#       load
#       save
#   menu help
#       rules
#       interactions (racc claviers)
#       About
#   menu settings
#       ruleset
#       retourn plateau
#       IA settings
'''

###################################### IMPORTS ######################################

from gess_rules import *
from gess_basics import *
from gess_ia import IA
from os import environ, mkdir, sep as slash
from os.path import isdir
#
# supprime msg bienvenue pygame lors de son import
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
# a faire entre imports os.environ et pygame
#
import pygame
from time import time


##################################### CONSTANTES #####################################
SAVE_DIR="savedGames/"          #dossier sauvegarde
IA_PLAYS=pygame.USEREVENT + 1   #seul evenement pygame cree: reception d'un coup joué par IA



###################################### CLASSES ######################################

class Goban:
    """La classe Goban est le plateau de jeu en tant qu'objet graphique. Il permet d'afficher le goban dans la fenetre de jeu.
        Un Goban ne connait pas d'etat de partie. Ils doivent toujours lui etre fournis en parametre
        La classe Game contient les elements de l'etat d'une partie"""
    def __init__(_,sqSize:int,vChkMode):
#        _.prevArrDbg=None
        _.sqSize=sqSize
        # GRAPHISME: surface de peinture et fond (planche + quadrillage)
        _.surface=pygame.Surface((sqSize*20+1,sqSize*20+1))
        _.fond=pygame.image.load("resources/board02.png")
        _.fond=pygame.transform.scale(_.fond,(sqSize*20+1,sqSize*20+1))
        for k in range(1,20):
            #tracer les lignes:
            pygame.draw.line(_.fond,(70,50,20),(_.sqSize,k*_.sqSize),(19*_.sqSize,k*_.sqSize)) #H
            pygame.draw.line(_.fond,(70,50,20),(k*_.sqSize,_.sqSize),(k*_.sqSize,19*_.sqSize)) #V
        _.abcisses=[]        #va contenir des objets text/font puis des objets rect
        _.ordonnees=[]
        # GRAPHISME: etiquettes lignes et colonnes
        font=pygame.font.Font('resources/YukiCode-Regular.otf',sqSize//2)
        for i in range(18):#les abcisses en majuscule
            _.abcisses.append(font.render(chr(ord("A")+i),True,(0,0,50)))
            _.ordonnees.append(font.render(chr(ord("a")+i),True,(0,0,50)))
        for i in range(18):#les rectancles de positionnements:
            _.abcisses.append(_.abcisses[i].get_rect())
            _.ordonnees.append(_.ordonnees[i].get_rect())
        # GRAPHISME: pierres
        _.pierreNoire=pygame.image.load("resources/pierreNoire.png").convert_alpha()
        _.pierreNoire=pygame.transform.scale(_.pierreNoire,(sqSize-1,sqSize-1))
        _.pierreBlanche=pygame.image.load("resources/pierreBlanche.png").convert_alpha()
        _.pierreBlanche=pygame.transform.scale(_.pierreBlanche,(sqSize-1,sqSize-1))
        _.pierreOmbre=pygame.image.load("resources/pierreOmbre.png").convert_alpha()
        _.pierreOmbre.set_alpha(192)
        shadowsize=round((_.sqSize-1)*1.135)  #l'ombre est obtenue avec un blur augmentant la taille de 13.5%
        _.pierreOmbre=pygame.transform.scale(_.pierreOmbre,(shadowsize,shadowsize))
        #emplacement centre de la la piece selectionne
        _.waldoSelection=pygame.Surface((_.sqSize-1,_.sqSize-1))
        _.waldoSelection.set_alpha(180)    #0=transparent; 255=visible
        _.waldoSelection.fill((255,255,100))    #pieces selectionnees en jaune clair
        #emplacement courant est valide
        _.waldoValid=pygame.Surface((_.sqSize*3-1,sqSize*3-1))
        _.waldoValid.set_alpha(141)    #0=transparent; 255=visible
        _.waldoValid.fill((100,255,100))      #waldo valide -> mvt autorisé -> vert clair
        #emplacement courant est invalide
        _.waldoInvalid=pygame.Surface((_.sqSize*3-1,_.sqSize*3-1))
        _.waldoInvalid.set_alpha(141)    #0=transparent; 255=visible
        _.waldoInvalid.fill((255,100,100))    #waldo invalide -> mvt interdit -> rouge clair
        _.currentWaldo=None
        #case pierre morte
        #squareKilled=pygame.Surface((squareSize+1,squareSize+1))
        #squareKilled.set_alpha(51)    #0=transparent; 255=visible
        #squareKilled.fill((0,0,0))  #noir (assombrir cases avec pieces tuées)
        #cases destination autorisees
        _.squareAllowed=pygame.Surface((_.sqSize-1,_.sqSize-1))
        _.squareAllowed.set_alpha(181)    #0=transparent; 255=visible
        _.squareAllowed.fill((100,100,250)) #bleu pour les cases autorisées
    def getSquare(_,pos):
        """Goban.getSquare(pos) recoit un param pos en pixels en (x,y) dans le goban. Renvoie la case correspondante ou None si hors Goban """
        posx,posy=pos
        if posx<0 or posy<0 or posx%_.sqSize==0 or posy%_.sqSize==0:
            return(None)    #si hors cadre ou pile sur une ligne.
        posx=posx//_.sqSize - 1
        posy=posy//_.sqSize - 1
        if posx>18 or posy>18:
            return(None)
        return((posx,posy)) #entre -1 et 18 inclus
    def drawPiece(_,grille):
        '''creer un sprite (translucide) de la piece a deplacer'''
        piece=pygame.Surface((3*_.sqSize-1,3*_.sqSize-1),pygame.SRCALPHA)
        piece.set_alpha(110)
        ombre=piece.copy()
        for j in range(3):
            for i in range(3):
                col=grille.shapePSel(i-1,j-1)
                if col==BLANC:
                    piece.blit(_.pierreBlanche,(i*_.sqSize,j*_.sqSize))
                elif col==NOIR:
                    piece.blit(_.pierreNoire,(i*_.sqSize,j*_.sqSize))
                if col:
                    ombre.blit(_.pierreOmbre,(i*_.sqSize,j*_.sqSize))
        return(piece,ombre)
    def draw(_,grille,caseSouris):
        '''dessine le goban en enlevant la piece selectionnée'''
        _.surface.blit(_.fond,(0,0))        #la planche avec les lignes
        #tracer coords#   TODO:gerer retournement des coords
        for k in range(18):
            #tracer les coords
            _.abcisses[k+18].center=((k+1.5)*_.sqSize,round(.5*_.sqSize))
            _.surface.blit(_.abcisses[k],_.abcisses[k+18])
            #j=(20-k) if plr==1 else k
            _.ordonnees[k+18].center=(round(.5*_.sqSize),(k+1.5)*_.sqSize)
            _.surface.blit(_.ordonnees[k],_.ordonnees[k+18])
        #creer une surface transparente pour dessiner les pierres
        surfPierres=pygame.Surface(_.surface.get_size(),pygame.SRCALPHA)
#        surfPierres.set_colorkey((0,0,0))
#        surfPierres=surfPierres.convert_alpha()
        surfWaldos=surfPierres.copy()
        #tracer les ombres et pierres sauf celles de depart
        rectOmbre=_.pierreOmbre.get_rect()
        depart=grille.pieceSelect
        for j in range(18):
            for i in range(18):
                if depart and -2<depart[0]-i<2 and -2<depart[1]-j<2:
                    continue    #on ne dessine pas les pierres de la piece selectionnée
                if grille[j][i]:
                    rectOmbre.topright=((i+2)*_.sqSize-1,(j+1)*_.sqSize+1)
                    _.surface.blit(_.pierreOmbre,rectOmbre)
                    if grille[j][i]==1:
                        pierre=_.pierreBlanche
                    else:
                        pierre=_.pierreNoire
                    surfPierres.blit(pierre,(1+(i+1)*_.sqSize,1+(j+1)*_.sqSize))
        if caseSouris:
            #il y a une caseSouris. Lui attribuer waldoValid ou waldoInvalid
            i,j=(1+i*_.sqSize for i in caseSouris) #i,j reglés pour bloc 3x3 a l'arrivee
            if depart:
                #il y a aussi un depart
                surfWaldos.blit(_.waldoValid if caseSouris in grille.mouvementsPSel()+[depart] else _.waldoInvalid,(i,j))
#                if _.prevArrDbg!=caseSouris:
#                 print("Goban.draw[D&A]:arrivee,mvs",coords2str(caseSouris),grille.mouvementsPSel(),flush=True)
                surfPiece,surfOmbre=_.drawPiece(grille)
                surfPierres.blit(surfPiece,(i,j))
                _.surface.blit(surfOmbre,(i,j))
            else:
                surfWaldos.blit(_.waldoValid if caseSouris in grille.getPieces(grille.joueur) else _.waldoInvalid,(i,j))
        if depart:
            #une piece est selectionnee. On trace le waldo du point de depart
            i,j=(1+(i+1)*_.sqSize for i in depart)
            surfWaldos.blit(_.waldoSelection,(i,j))
            #on trace ses mouvements possibles
            for i,j in grille.mouvementsPSel():
                surfWaldos.blit(_.squareAllowed,(1+(i+1)*_.sqSize,1+(j+1)*_.sqSize))
        _.surface.blit(surfPierres,(0,0))
        surfWaldos.set_alpha(120)
        _.surface.blit(surfWaldos,(0,0))
        #TODO: tourner le goban si player 1
#        _.prevArrDbg=caseSouris


class Game:
    '''Cette classe Game designe une partie avec ses parametres: un Goban pour afficher, une Grille pour la situation en cours et eventuellement une IA ainsi qu'une fenetre de jeu/instance pygame et divers parametres'''
    def __init__(_,taille:tuple,joueurs:list,grille:Grille=None):
        #init fenetre jeu
        pygame.init()
        pygame.display.set_caption("Gess - the Round Chess")
        pygame.display.set_icon(pygame.image.load('resources/icone.png'))
        #machins logiques
        if grille:
            _.grille=grille
        else:
            _.grille=Grille()
        _.vieilleGrille=None        #pour undo 1 rang. acceptable.
        #init les machins a dessiner hors du goban
        #scr est la surface d'affichage de la fenetre.
        _.scr=pygame.display.set_mode(taille)
        #fond est le fond de la fenetre (blanc)
        _.fond=pygame.Surface(taille)
        _.fond.fill((255,255,255))
        squareSize=min(taille)//20
        if squareSize*20>min(taille)-5:
            squareSize-=1
        #initialiser le goban
        _.gobanOffset=tuple((min(taille)-squareSize*20-1)//2 for i in range(2))    #coord 2D -> 2 fois la mm valeur
        _.goban=Goban(squareSize,vChkMode="ennemiSeul")   #TODO: vCheckMode est un mode de verification de victoire. Ca va dans un ruleset de gess_rules.py.
        _.fps=20
        _.tour=1
        _.popup=None        #popup de menu/resultat/whatever a afficher. Inutilisé a ce jour
        _.clickPos=None     #endoit cliqué (notamment case goban) avant relachement.
        _.curSquare=None    #emplacement souris pour waldo
        #_.validMove=None
        _.clock=pygame.time.Clock()
        #event related_Stuff
        _.firstFrame=True
        _.ctrlPressed=0   #un par touche ctrl enfoncee
        _.shiftPressed=0   #un par touche shift enfoncee
        _.ia=None
        #on recoit un param joueurs de la forme [<nom noir>,<nom blanc>] mais on veut pourvoir l'utiliser avec _.joueurs[couleur] sachant que couleur parmi BLANC=1,NOIR=-1
        _.joueurs=["",joueurs[1],joueurs[0]]    #_.joueurs[_.grille.joueur] est le joueur en cours
        _.joueurs=[None,joueurs[1],joueurs[0]]
        ialevels=None if not joueurs[0].startswith("IA_") else int(joueurs[0][3:])      #None pour humain, nb>0 pour IA
        ialevels=[ialevels, None if not joueurs[1].startswith("IA_") else int(joueurs[1][3:])]
        assert(0 not in ialevels)
        if any(ialevels):
            #les params de creation de l'IA sont: la situation du jeu, la fonction a appeler quand l'IA a choisi son coup, les niveaux d'IA'
            _.ia=IA(_.grille,_.recoitCoup,ialevels)
            _.ia.start()
        _.debutTour=time()  #jamais utilisé.
    def recoitCoup(_,depart,arrivee):
        pygame.event.post(pygame.event.Event(IA_PLAYS,{"depart":depart,"arrivee":arrivee}))
        #TODO: verifier mv valide. Dans la negative, renvoyer False puis envoyer la grille courant par _.ia.envoieSGI
        return(True)
    def draw(self):
        self.scr.blit(self.fond,(0,0))
        #print("Game.draw:curSquare",coords2str(_.curSquare),flush=True)
        self.goban.draw(self.grille,self.curSquare)
        self.scr.blit(self.goban.surface,self.gobanOffset)
        self.firstFrame=False
        pygame.display.update()
    def undo(_):
        if _.vieilleGrille:
            _.setGrille(_.vieilleGrille)
            _.grille.deselectPiece()
            print("undoing")
            if _.tour==1:
                _.tour=2
            else:
                _.tour-=1
        else:
            print("nothin to undo")
    def quit(_):
            pygame.quit()
            #_.ia.slowKill()
            exit(0)
    def sauve(_,dir,askName):
        if not isdir(dir):
            mkdir(dir)
        if askName:
            fname=input("fichier a sauver:")
        else:
            fname="lastsave"
        _.grille.sauve(dir,fname)
        print("fichier %s sauvé"%fname)
    def charge(_,dir,askName):
        if askName:
            fname=input("fichier a charger:")
        else:
            fname="lastsave"
        g=Grille.charge(dir,fname)
        if g:
            _.setGrille(g)
            print("fichier %s chargé"%fname)
            _.tour=1 if g.joueur==NOIR else 2
        else:
            print("fichier %s non trouvé"%fname)
    def setGrille(_,g):
        _.vieilleGrille,_.grille = _.grille,g
        #envoyer a l'ia la nouv grille
        if _.ia:
            _.ia.recoitSGI(g)
    def move(_,arrivee):
        nvGrille=Grille(_.grille)
        chnMouv="Dernier coup joué: "+mv2str(_.grille.pieceSelect,arrivee)
#        print("Game.move[try]:sel,arrivee",coords2str(nvGrille.pieceSelect),coords2str(arrivee),flush=True)
        if not nvGrille.move(arrivee):
            print("mvt invalide")
            print(coords2str(_.grille.pieceSelect))
            print(chnMouv)
            return  #on ne fait rien si mouvement invalide
        _.setGrille(nvGrille)
        _.tour+=1
        #test de fin de partie
        v=chkVictoire(_.grille)#,joueur)
        if v:
            gagnant="%s ( %s )"%(_.joueurs[v],"Blanc" if v==1 else "Noir")
            chnVictoire="Yeah! %s gagne"%gagnant
            debugDisplayGrille(_.grille,[chnVictoire,chnMouv])
            #ia.slowKill()  #pas la peine. le exit ci dessous va generer une exception
            _.quit()
        elif v==PAT:
            print("Oooooh! C'est un pat. Blanc gagne par le komi")
            exit(0) #declencher un arret du prog (est une exception)
    def manageEvent(_,evt):
        """
        @brief blabla
        :param evt: p_evt: evenement a traiter ou negliger
        :type evt: t_evt:pygame.event
        :returns: void
        """
        iaALeTrait=_.joueurs[_.grille.joueur].startswith("IA_")
        if evt.type==pygame.QUIT:
            _.quit()
        elif evt.type==IA_PLAYS:
            if iaALeTrait:
                _.grille.selectPiece(evt.depart)
                _.move(evt.arrivee)
            else:
                #TODO: ia.recoitSGI(grille)... il a l'air aux fraises
                pass
        elif evt.type==pygame.MOUSEMOTION and not _.firstFrame:
            _.curSquare=_.goban.getSquare(vectDiff(_.gobanOffset,evt.pos))
            #TODO: Gerer si ya popup ou si (curSq is None and ya qqchose a mouseoverer hors goban
        elif evt.type==pygame.MOUSEBUTTONDOWN and evt.button==1:
            if iaALeTrait:
                return
            _.clickPos=_.curSquare  #memoriser la case dernierement survolee ou None
        elif evt.type==pygame.MOUSEBUTTONUP and evt.button==1:
            if iaALeTrait:
                return
            if _.clickPos==_.curSquare and _.clickPos!=None:
                piece=_.grille.pieceSelect
                #print("Game.manageEvent[MBup]:clickPos,piece",coords2str(_.clickPos),coords2str(piece),flush=True)
                if piece:
                    if piece==_.clickPos:
                        #on deselectionne la piece
                        _.grille.deselectPiece()
                    else:
                        _.move(_.clickPos)  #incl verif mvt valide & victoire
                else:
                    _.grille.selectPiece(_.clickPos)
        elif evt.type == pygame.KEYDOWN:
            if evt.key == pygame.K_s and _.ctrlPressed==1 and _.shiftPressed<2:
                #save game
                _.sauve(SAVE_DIR,_.shiftPressed==1)
            elif evt.key == pygame.K_l and _.ctrlPressed==1 and _.shiftPressed<2:
                #load game  #TODO: gerer load game et IA (actuellement desactivé par "and False")
                _.charge(SAVE_DIR,_.shiftPressed==1)
            elif evt.key in [pygame.K_LSHIFT,pygame.K_RSHIFT] :
                _.shiftPressed+=1
            elif evt.key in [pygame.K_LCTRL,pygame.K_RCTRL] :
                _.ctrlPressed+=1
            elif evt.key == pygame.K_z and _.ctrlPressed==1 and _.shiftPressed==0:
                #undo       #TODO: gerer undo et IA (actuellement desactivé par "and False")
                _.undo()
        elif evt.type == pygame.KEYUP:
            if evt.key in [pygame.K_LCTRL,pygame.K_RCTRL] :
                _.ctrlPressed-=1
            elif evt.key in [pygame.K_LSHIFT,pygame.K_RSHIFT] :
                _.shiftPressed-=1
    def mainLoop(_):
        for evt in pygame.event.get():
            _.manageEvent(evt)
        _.draw()
        _.firstFrame=False
        _.clock.tick(_.fps)

###################################### LE PROGRAMME ######################################


if __name__=="__main__":
    try:
        ga=None
        joueurs=["IA_12","Humain"]
        ga=Game((1200,700),joueurs)
        while True:
            ga.mainLoop()
    finally:
        print("fin de jeu. Tentative tuage ia")
        if ga and ga.ia:
            ga.ia.slowKill()

