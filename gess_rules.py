# -*- coding: utf-8 -*-

'''
# Description:
   Regles du jeu de gess
   Utilisable tel quel en mode texte PvP ou en tant que librairie via un GUI tel que gess.py

# Licence:
   GPL V3 (https://www.gnu.org/licenses/gpl-3.0.en.html)

 Auteur:
   perdrive@gmx.com

# Version: 1.2.0
    Release: 31/01/2023

# Nouveautés:
    integration IA en mesure de jouer via gess.py

# TODO (optionnel):
   gerer des rulesets
   gerer pat (ne restent que 2 rois SANS SITUATION DE PRISE DIRECTE)
                                                            . ◆ .
   regles avancées en 24x24 avec des cavaliers de la forme  ◆ . ◆
                                                            . ◆ .
'''


from os import sep as slash
from os.path import isfile
from gess_basics import *


class Grille(list):
    def __init__(_,grille=None,ruleset=None):   #TODO: gerer des rulesets
        if grille:
            #a partir d'une autre grille
            for j in range(18):
                _.append(grille[j].copy())
            _.ruleset=grille.ruleset
            _.joueur=grille.joueur  #qui a le trait (type int)
            _.rings=vraieCopie(grille.rings)
            _.pieces=vraieCopie(grille.pieces)            #dict {clr:[pieces]} ou None
            _.pierres=vraieCopie(grille.pierres)          #liste [0,nb_blc_nb_noir] ou None
            _.pieceSelect=vraieCopie(grille.pieceSelect)  #centre de la piece selectionnée ou None
            _.movesPSel=vraieCopie(grille.movesPSel)      #liste mouvements possible piece selectionnee
            _.collisPSel=vraieCopie(grille.collisPSel)    #hash des positions arrivee->collisions (si existent)
        else:
            #nouvelle partie
            _.append([0,1,0, 1,0,1, 1,1,1, 1,1,1, 1,0,1, 0,1,0])
            _.append([1,1,1, 0,1,0, 1,0,1, 1,1,1, 0,1,0, 1,1,1])
            _.append([0,1,0, 1,0,1, 1,1,1, 1,1,1, 1,0,1, 0,1,0])
            for j in range(15):
                _.append([0]*18)
            for i in range(len(_)):
                _[-1][i]=-1*_[2][i]
                _[-3][i]=-1*_[2][i]
                _[-2][i]=-1*_[1][i]
            for i in range(6):
                _[5][3*i+1]=1
                _[-6][3*i+1]=-1
            _.joueur=NOIR   #qui a le trait
            _.ruleset=ruleset
            #TODO: passer les pieces en list au lieu de dict. Pour faire comme les autres ci dessous
            _.pieces=None   #sera un hash [NOIR|BLANC] : [list centres pieces]
            _.pierres=[0,43,43]
            _.rings=[None,[(7,1)],[(7,16)]]
            _.pieceSelect=None
            _.movesPSel=[]
            _.collisPSel={}
    def copy(_):
        return(Grille(_))
    def __eq__(_,g):
        return(all([all([_[i][j]==g[i][j] for j in range(18)]) for i in range(18)]) and g.joueur==_.joueur)
    def __bytes__(_):
        id=grille2id(_)
        return(gid2bytes(id))
    def sauve(_,path=".",fname="grille"):
        #s'assurer de la terminaison ".geg" du nom fichier, nom disponible
        if fname.endswith(".geg"):
            fname=fname[:-4]        #j'enleve le .geg
        #pas d'ecrasement sauf si le nom de fichier commence par "last"
        if (not fname.startswith("last")) and isfile(path+slash+fname+".geg"):
            n=0
            while(isfile("%s_%i.geg"%(path+slash+fname,n))):
                n+=1
            fname="%s_%i.geg"%(fname,n)
            print("ATTENTION: fichier enregistré sous "+fname)
        else:
            fname+=".geg"   #et je remet le .geg
        f=open(path+slash+fname,"wb")
        f.write(bytes(_))
        f.close()
    def charge(path,fname):
        """methode abstraite. S'utilise Grille.charge("machin.geg")
        Renvoie une Grille"""
        if not fname.endswith(".geg"):
            fname+=".geg"
        try:
            f=open(path+slash+fname,"rb")
            id=file2gid(f)
            return(id2grille(id))
        except:
            return(None)
    def getVal(_,x,y=None):
        if y==None:
            x,y=x
        if 18>x>=0 and 18>y>=0:
            return(_[y][x])
        return(0)
    def isRingPattern(_,oripos,testpos):
        #oripos est le point en h a g d'un ring potentiel(pattern). testpos est le point a tester
        #val retour: 0: ne sait pas. 1 est un ring. -1 n'est pas un ring
        v=vectDiff(oripos,testpos)  #balayage hor -> v[1]>=0.
        if v[0]<0 or v[0]>2:
            return(0)    #on n'est pas dans la colonne du pattern. ni validé ni refusé
        nc=_.getVal(testpos)    #nouvelle couleur: celle de la case testee
        if v==(1,1) :
            if nc:
                return(-1)  #un ring aurait ete vide au centre. Refusé
            return(0)   #la case centrale est bien vide. Il faut continuer a parcourir le pattern
        retval= nc==_.getVal(oripos) #case testée vaut elle point en h a g?
        if retval and v==(2,2):
            return(1)   #on est arrivé au bout. tout va bien. Validé.
        return(int(retval)-1) #renvoie refus(-1) si etait False (donc couleur NOK) et "faut voir" (0) sinon
    def getRings(_,plr):
        try:
            return(_.rings[plr])
        except:
            pass
        _.rings=[None,[],[]]    #On peut lappeler avec [BLANC==1] ou [NOIR==-1%=2]
        ringPatterns=[]#rings potentiels. notés par le tuple de son point en haut a gauche.
        for j in range(18):
            for i in range(18):
                k=0
                while k<len(ringPatterns):
                    pattern = ringPatterns[k]
                    isValid = _.isRingPattern(pattern,(i,j))
                    if isValid==0:
                        k+=1    #on ne sait pas encore. Au pattern suivant.
                    elif isValid==-1:
                        ringPatterns.pop(k) #negatif. on elimine
                    elif isValid==1:
                        centre=(i-1,j-1)#vectDiff((-1,-1),oripos)
                        _.rings[_.getVal(i,j)].append(centre) #positif. On garde
                        ringPatterns.pop(k) #Traité. on elimine
                    else:
                        assert(False)
                if _[j][i]!=0 and i<16 and j<16:
                    ringPatterns.append((i,j))
        return(_.rings[plr])
    #renvoie liste de pos (x,y)
    def getPieces(_,owner=0):
        if owner==0:
            owner=_.joueur
        try:
            return(_.pieces[owner])
        except:
            pass
        savec=[None,set(),set()]    #unused=0,blanc=1,noir=-1
        ssans=[None,set(),set()]
        def note(i,j):
            #on note qu'il pourrait y avoir une piece de la couleur centree sur une des 8 cases adjacentes
            #et qu'il ne peut y avoir de piece de la couleur opposéee sur cette case et les 8 adjacentes
            clr=_.getVal(i,j)
            for y in range(j-1,j+2):
                for x in range(i-1,i+2):
                    ssans[clr*-1].add((x,y))
                    if (x,y)!=(i,j):
                        savec[clr].add((x,y))
        for j in range(18):
            for i in range(18):
                if _[j][i]:
                    note(i,j)
        _.pieces={}
        _.pieces[NOIR]=list(savec[NOIR].difference(ssans[NOIR]))
        _.pieces[BLANC]=list(savec[BLANC].difference(ssans[BLANC]))
        return(_.pieces[owner])
    def getPierres(_,plr):
        try:
            return(_.pierres[plr])
        except:
            pass
        _.pierres=[0]*3
        for j in range(18):
            _.pierres[NOIR]+=_[j].count(NOIR)
            _.pierres[BLANC]+=_[j].count(BLANC)
        return(_.pierres[plr])
    def deselectPiece(_):
        _.pieceSelect=None
        _.movesPSel=[]
        _.collisPSel={}
    def selectPiece(_,pos):
        if pos not in _.getPieces():
            return(False)
        if _.pieceSelect:
            _.deselectPiece()   #oublier les moves et collisions
        _.pieceSelect=pos
        return(True)
    def directionsPSel(_):
        assert(_.pieceSelect)
        for sx in range(-1,2):
            for sy in range(-1,2):
                if (sx,sy)==(0,0):
                    continue
                if _.shapePSel(sx,sy):
                    yield((sx,sy))
    def shapePSel(_,x,y=None):
        assert(_.pieceSelect)
        if y==None:
            x,y=x
        assert(x in [-1,0,1])
        assert(y in [-1,0,1])
        pos=vectDiff((-x,-y),_.pieceSelect)
        return(_.getVal(pos))
    def move(_,pos):
        if pos not in _.mouvementsPSel():
            return(False)
        #sauver piece et nettoyer zone depart
        y=haut=_.pieceSelect[1]-1
        gauche=_.pieceSelect[0]-1
        shape=[]
        for j in range(3):
            if (0<=y<18):
                shape.append([])
                x=gauche
                for i in range(3):
                    if (0<=x<18):
                        shape[-1].append(_[y][x])
                        _[y][x]=0
                    else:
                        shape[-1].append(0)
                    x+=1
            else:
                shape.append([0,0,0])
            y+=1
        #placer la piece a l'arrivee
        y=haut=pos[1]-1
        gauche=pos[0]-1
        for j in range(3):
            if 0<=y<18:
                x=gauche
                src=shape[j]
                dst=_[y]
                for i in range(3):
                    #max(pos[0]-1,0),min(pos[0]+2,18)):
                    if 0<=x<18:
                        dst[x] = src[i]
                    x+=1
            y+=1
        #On oublie les pieces valides pour les recalculer au besoin
        _.pieces=None
        _.rings=None
        _.pierres=None
        #on oublie la piece selectionnee
        _.deselectPiece()
        #changer de joueur
        _.joueur*=-1
        return(True)
        """    def estPieceValide(_,pos):
            return(plr)
        return(0)"""
    def mouvementsPSel(_):
        """methode renvoyant les mouvements possibles de la piece selectionnée.
        Renvoie vide si pas de piece selectionnee.
        Renvoie resultat deja connu si dispo.
        Sinon, calcule les champs movesPSel et collisPSel"""
        if not _.pieceSelect:
            return([])
        if _.movesPSel:
            return(_.movesPSel)
        _.movesPSel=[]
        _.collisPSel={}
        maxDist=17 if _.getVal(_.pieceSelect)==_.joueur else 3
        for sx,sy in _.directionsPSel():
            dist,collis=_.collidePath(sx,sy,maxDist)
            for d in range(1,dist+1):
                _.movesPSel.append(vectDiff((-d*sx,-d*sy),_.pieceSelect))
            if collis:
                _.collisPSel[vectDiff((-d*sx,-d*sy),_.pieceSelect)]=collis
        return(_.movesPSel)
    def collidePath(_,sx,sy,dist):
        assert( sx!=0 or sy!=0 )
        assert( dist>0 )
        assert( sx in [0,1,-1] )
        assert( sy in [0,1,-1] )
        assert( _.pieceSelect )
        px,py=_.pieceSelect
        collisions=[]
        def appendIfCollide(x,y):
            if _.getVal(x,y):
                collisions.append((x,y))
                return(True)
            return(False)
        collided=False
        if sx==0:   #deplacement vertical
            xm=max(0,px-1)
            xM=min(18,px+2)
            for d in range(dist):
                j=py+(d+2)*sy
                if not 0<=j<18:
                    break
                for i in range(xm,xM):
                    collided|=appendIfCollide(i,j)
                if collided:
                    break
        elif sy==0: #deplacement horizontal
            ym=max(0,py-1)
            yM=min(18,py+2)
            for d in range(dist):
                i=px+(d+2)*sx
                if not 0<=i<18:
                    break
                for j in range(ym,yM):
                    collided|=appendIfCollide(i,j)
                if collided:
                    break
        else:   #deplacement diagonal
            for d in range(dist):
                xm=px+(d+2)*sx
                ym=py+(d+2)*sy
                collided|=appendIfCollide(xm,ym)
                for l in range(1,3):
                    collided|=appendIfCollide(xm-l*sx,ym)
                    collided|=appendIfCollide(xm,ym-l*sy)
                if collided or xm<0 or xm>17 or ym<0 or ym>17:
                    break
        return(d+1,collisions)

##sauvegarde chargement de grille
def grille2id(grille):
    retval=0
    pow3=1
    for j in range(18):
        for i in range(18):
            retval+=(grille[j][i]+1)*pow3
            pow3*=3
    if grille.joueur>0:
        return(retval)
    return(-retval-1)

def id2grille(id):
    if id<0:
        plr=-1
        id=-id-1
    else:
        plr=1
    grille=Grille()
    grille.joueur=plr
    for j in range(18):
        for i in range(18):
            id,g=divmod(id,3)
            grille[j][i]=g-1
    grille.pieces=None
    grille.pierres=None
    grille.rings=None
    return(grille)

def gid2bytes(id):
    if id<0:
        id=-id
        retval=b'\00'
    else:
        retval=b'\02'
    for i in range(65):
        id,r=divmod(id,256)
        retval+=r.to_bytes(1,'little')
    assert(id==0)
    return(retval)

def bytes2gid(bb):
    sg=int(bb[0])-1
    retval=0
    p256=1
    for i in range(1,66):
        retval+=p256*bb[i]
        p256*=256
    return(sg*retval)

def file2gid(f):
    bb=f.read(66)
    return(bytes2gid(bb))

def gid2file(f,id):
    f.write(gid2bytes(id))




#chkVictoire doivent renvoyer NOIR si N gagne, BLANC si B gagne, PAT si exeko, INDE6 si partie non finie
#NE PAS APPELER APRES AVOIR JOU€ ET AVANT LE CHANGEMENT DE TOUR.
def chkVictoire(grille,plr=0):
    if plr==0:
        plr=grille.joueur
    enmi=-1*plr
    if grille.getRings(enmi):
        if grille.getRings(plr):
            if grille.getPierres(NOIR)==grille.getPierres(BLANC)==8:
                return(PAT)
            return(INDE6)
        return(enmi)#enmi gagne si plr n'a pas de ring a la fin de son tour
    return(plr) #on gagne si on a toujours son roi et pas l'ennemi


def coords2str(x,y=None):
    try:
        x,y=x
    except:
        if x==None:
            return(None)
    if x==-1:
        x="Z"
    else:
        x=chr(ord("A")+x)
    if y==-1:
        y="z"
    else:
        y=chr(ord("a")+y)
    return(x+y)

strCoords=coords2str

######################################################
# Jeu en mode texte

def str2coords(chn):
    try:
        x=-1 if chn[0]=="Z" else ord(chn[0])-ord("A")
        y=-1 if chn[1]=="z" else ord(chn[1])-ord("a")
        return(x,y)
    except:
        return(None)


def da2mv(orig,dest):
    return(orig[0],orig[1],dest[0],dest[1])

def mv2da(mv):
    return((mv[:2],mv[2:]))

def mv2str(dep,arr=None):
    if arr==None:
        dep,arr=mv2da(dep)
    return("%s->%s"%(strCoords(dep),strCoords(arr)))

#renvoie la grille sous forme de liste de chaines -> plateau avec coords
def grille2txt(g:Grille):
    a=ord('a')
    A=ord('A')
    #retval=["   "+" ".join(str(i) for i in range(10)) + "".join(str(i) for i in range(10,18))]
    hdr="   "+" ".join(chr(i+A) for i in range(18))
    retval=[hdr]
    for y in range(18):
        sy=" "+chr(y+a)
        retval.append(sy+" "+" ".join(list("." if g[y][x]==0 else "◇" if g[y][x]==NOIR else "◆" for x in range(18)))+sy)
    retval.append(hdr)
    return(retval)


#prend en param une grille et une liste (max 19) de chaines a afficher en face. Penser a sa largeur de console
def debugDisplayGrille(g:Grille,l:list=['']):
    #rien
    g2t=grille2txt(g)
    print(g2t[0])
    for i in range(min(19,len(l))):
        print(g2t[i+1]+"\t"+l[i])
    for i in range(i+2,20):
        print(g2t[i])




if __name__=="__main__":
    print("Gess mode text")
    g=Grille()
    while chkVictoire(g)==INDE6:
        debugDisplayGrille(g)
        clr="Noir" if g.joueur==NOIR else "Blanc"
        src=False
        while not src:
            src=input("Selection %s: "%clr)
            src=str2coords(src)
            if not g.selectPiece(src):
                src=False
        dst=False
        while not dst:
            dst=input("Destination %s: "%clr)
            if dst==src:
                g.deselectPiece()
            dst=str2coords(dst)
            if not g.move(dst):
                dst=False
    res=chkVictoire(g)
    if res==NOIR:
        res=["Victoire Noir"]
    elif res==BLANC:
        res=["Victoire Blanc"]
    elif res==PAT:
        res=["Pat. Bravo a Blanc"]
    else:
        res=["Huh? wot?",str(res)]
    debugDisplayGrille(g,res)

